# Variables pour le module ASG

# Taille minimale de l'ASG
variable "asg_min_size" {
  description = "Taille minimale du groupe de mise à l'échelle automatique"
}

# Taille maximale de l'ASG
variable "asg_max_size" {
  description = "Taille maximale du groupe de mise à l'échelle automatique"
}

# Variables pour le Load Balancer
variable "elb_security_groups" {
  description = "Groupes de sécurité pour le Load Balancer"
  default     = []
}