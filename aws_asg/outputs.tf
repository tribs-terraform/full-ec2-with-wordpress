# Sortie pour l'adresse IP du Load Balancer
output "load_balancer_ip" {
  value = aws_elb.my_elb.dns_name
}
