# Création du groupe de mise à l'échelle automatique (ASG)
resource "aws_autoscaling_group" "my_asg" {
  min_size = var.asg_min_size
  max_size = var.asg_max_size
  launch_configuration = aws_launch_configuration.my_launch_configuration.name
  vpc_zone_identifier  = [aws_subnet.my_subnet.id]
  tags = {
    Name = "my_asg"
  }
}

# Configuration de lancement pour l'ASG
resource "aws_launch_configuration" "my_launch_configuration" {
  image_id = var.ami_id
  instance_type = var.instance_type
  security_groups = [aws_security_group.my_security_group.name]
  key_name = var.key_name
}

# Création du Load Balancer
resource "aws_elb" "my_elb" {
  name               = "my-elb"
  availability_zones = [aws_subnet.my_subnet.availability_zone]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  instances = [aws_autoscaling_group.my_asg.id]
}
