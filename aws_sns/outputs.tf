output "sns_topic_arn" {
  value = aws_sns_topic.my_topic.arn
  description = "ARN du sujet SNS"
}

output "sns_subscription_id" {
  value = aws_sns_topic_subscription.my_subscription.id
  description = "ID de l'abonnement SNS"
}
