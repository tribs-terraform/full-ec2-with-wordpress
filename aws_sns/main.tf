# Configuration d'AWS SNS
resource "aws_sns_topic" "cloudwatch_alarm" {
  name = "cloudwatch-alarm"
}

# Abonnement SNS pour les alertes CloudWatch
resource "aws_sns_topic_subscription" "cloudwatch_alarm_subscription" {
  protocol = "email"
  topic_arn = aws_sns_topic.cloudwatch_alarm.arn
  endpoint  = var.sns_email
}
