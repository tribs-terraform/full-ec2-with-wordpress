# Variables pour le module AWS SNS

# Adresse e-mail pour les notifications SNS
variable "sns_email" {
  description = "Adresse e-mail pour les notifications SNS"
}
