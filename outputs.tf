# Outputs
output "vpc_id" {
  value = module.vpc.vpc_id
}

output "ec2_instance_ip" {
  value = module.ec2.instance_ip
}

output "rds_endpoint" {
  value = module.rds.rds_endpoint
}

output "asg_name" {
  value = module.asg.asg_name
}

output "route53_domain_name" {
  value = module.aws_route53.domain_name
}
