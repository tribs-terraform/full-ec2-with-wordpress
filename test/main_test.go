// Test de l'infrastructure avec Terratest
package test

import (
  "testing"
  "github.com/gruntwork-io/terratest/modules/terraform"
)

func TestTerraform(t *testing.T) {
  t.Parallel()

  options := &terraform.Options{
    TerraformDir: "../",
  }

  defer terraform.Destroy(t, options)
  terraform.InitAndApply(t, options)
}
