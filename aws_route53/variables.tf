variable "domain_name" {
  description = "The domain name to use for Route 53"
  type        = string
}

variable "instance_ip" {
  description = "The IP address of the EC2 instance"
  type        = string
}

