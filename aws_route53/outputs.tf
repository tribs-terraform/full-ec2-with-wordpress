output "route53_zone_id" {
  value = aws_route53_zone.my_zone.id
  description = "ID de la zone Route 53"
}

output "route53_record_fqdn" {
  value = aws_route53_record.my_record.fqdn
  description = "Nom de domaine complet (FQDN) de l'enregistrement Route 53"
}
