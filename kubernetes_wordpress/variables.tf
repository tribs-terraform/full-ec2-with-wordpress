variable "db_host" {
  description = "Nom de l'hôte de la base de données RDS"
}

variable "db_username" {
  description = "Nom d'utilisateur pour la base de données RDS"
}

variable "db_password" {
  description = "Mot de passe pour la base de données RDS"
}

variable "db_name" {
  description = "Nom de la base de données RDS"
}
