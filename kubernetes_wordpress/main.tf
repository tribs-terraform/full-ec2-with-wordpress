provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_deployment" "wordpress" {
  metadata {
    name = "wordpress"
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        App = "wordpress"
      }
    }

    template {
      metadata {
        labels = {
          App = "wordpress"
        }
      }

      spec {
        container {
          image = "wordpress:latest"
          name  = "wordpress"

          env {
            name = "WORDPRESS_DB_HOST"
            value = var.db_host
          }

          env {
            name = "WORDPRESS_DB_USER"
            value = var.db_username
          }

          env {
            name = "WORDPRESS_DB_PASSWORD"
            value = var.db_password
          }

          env {
            name = "WORDPRESS_DB_NAME"
            value = var.db_name
          }
        }
      }
    }
  }
}
