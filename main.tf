# Configuration du fournisseur AWS
provider "aws" {
  region = var.aws_region
}

# Configuration du fournisseur Kubernetes
provider "kubernetes" {
  config_path = "~/.kube/config"
}

# Module pour la création du VPC
module "vpc" {
  source = "./modules/aws_vpc"
  aws_region = var.aws_region
  vpc_cidr = var.vpc_cidr
}

# Module pour la création des instances EC2
module "ec2" {
  source = "./modules/aws_ec2"
  ami_id = var.ami_id
  instance_type = var.instance_type
  key_name = var.key_name
}

# Module pour la création de la base de données RDS
# Utilisation d'AWS Secrets Manager pour le mot de passe
module "rds" {
  source = "./modules/aws_rds"
  db_storage = var.db_storage
  db_storage_type = var.db_storage_type
  db_engine = var.db_engine
  db_engine_version = var.db_engine_version
  db_instance_class = var.db_instance_class
  db_name = var.db_name
  db_username = var.db_username
  db_password = aws_secretsmanager_secret_version.db_password.secret_string
  db_parameter_group = var.db_parameter_group
  db_skip_final_snapshot = var.db_skip_final_snapshot
}

# Module pour la création du groupe de mise à l'échelle automatique (Auto Scaling Group)
module "asg" {
  source = "./modules/aws_asg"
  asg_min_size = var.asg_min_size
  asg_max_size = var.asg_max_size
}

# Module pour la création des rôles et utilisateurs IAM
module "iam" {
  source = "./modules/aws_iam"
}

# Module pour la configuration d'AWS Config
module "aws_config" {
  source = "./modules/aws_config"
}

# Module pour la configuration d'AWS CloudTrail
module "aws_cloudtrail" {
  source = "./modules/aws_cloudtrail"
}

# Module pour la configuration d'AWS CloudWatch
module "aws_cloudwatch" {
  source = "./modules/aws_cloudwatch"
}

# Module pour la configuration d'AWS SNS
module "aws_sns" {
  source = "./modules/aws_sns"
}

# Module pour le déploiement de WordPress sur Kubernetes
module "kubernetes_wordpress" {
  source = "./modules/kubernetes_wordpress"
  db_host = module.rds.db_host
  db_name = var.db_name
  db_user = var.db_username
  db_pass = aws_secretsmanager_secret_version.db_password.secret_string
}

# Module pour la configuration d'AWS Route 53
module "aws_route53" {
  source = "./modules/aws_route53"
  domain_name = var.route53_subdomain
  load_balancer_ip = module.asg.load_balancer_ip
}
