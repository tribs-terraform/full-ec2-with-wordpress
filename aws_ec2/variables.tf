# Variables pour le module EC2

# ID de l'AMI pour l'instance EC2
variable "ami_id" {
  description = "ID de l'AMI pour l'instance EC2"
}

# Type de l'instance EC2
variable "instance_type" {
  description = "Type de l'instance EC2"
}

# Nom de la clé pour l'instance EC2
variable "key_name" {
  description = "Nom de la clé pour l'instance EC2"
}
