output "ec2_instance_id" {
  value = aws_instance.my_instance.id
  description = "ID de l'instance EC2"
}

output "ec2_public_ip" {
  value = aws_instance.my_instance.public_ip
  description = "Adresse IP publique de l'instance EC2"
}

output "ec2_private_ip" {
  value = aws_instance.my_instance.private_ip
  description = "Adresse IP privée de l'instance EC2"
}

output "key_name" {
  value = aws_instance.my_instance.key_name
  description = "Nom de la clé SSH utilisée pour l'instance EC2"
}
