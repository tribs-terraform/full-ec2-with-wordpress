# Déploiement de WordPress sur AWS avec Terraform, Kubernetes et RDS

Ce projet utilise Terraform pour déployer une architecture AWS complète comprenant un cluster Kubernetes pour WordPress, une base de données RDS, et divers autres services AWS pour la surveillance et la sécurité.

## Architecture AWS Déployée

- **VPC**: Un VPC est créé avec des sous-réseaux publics et privés.
- **EC2**: Deux instances EC2 sont déployées et load-balancées.
- **RDS**: Une instance RDS MySQL est déployée pour la base de données WordPress.
- **Kubernetes**: Un cluster Kubernetes est déployé sur les instances EC2.
- **IAM**: Des rôles et des utilisateurs IAM sont créés avec des permissions minimales.
- **SNS**: Un service de notification pour les alertes.
- **CloudWatch**: Pour la surveillance et les logs.
- **CloudTrail**: Pour le suivi des activités.
- **AWS Config**: Pour vérifier la conformité de l'infrastructure.

## Prérequis

- Terraform installé
- Compte AWS
- AWS CLI configuré
- kubectl installé (pour interagir avec le cluster Kubernetes)

## Déploiement

### Exécution Locale

1. Configurez vos identifiants AWS via la AWS CLI ou les variables d'environnement.
2. Configurez les variables nécessaires dans le fichier `variables.tf` à la racine et dans les modules.
3. Exécutez `terraform init` pour initialiser le projet.
4. (Optionnel) Exécutez `terraform plan` pour visualiser les changements avant l'application.
5. Exécutez `terraform apply` pour déployer l'infrastructure sur AWS.

## Configuration

Accédez à l'URL de l'instance EC2 pour configurer WordPress.

## Configuration des Identifiants AWS et des Variables

- Configurez vos identifiants AWS et autres variables dans les fichiers `variables.tf` à la racine et dans les modules.

## Monitoring et Logs

- AWS CloudWatch est utilisé pour la surveillance et les logs.
- AWS CloudTrail est utilisé pour le suivi des activités.
- AWS Config est utilisé pour vérifier la conformité de l'infrastructure.

## Notifications

- AWS SNS est utilisé pour les notifications par e-mail en cas de problèmes. Configurez l'adresse e-mail dans le fichier `variables.tf` à la racine.

## Support

Pour toute question ou support, veuillez ouvrir une issue.

## Licence

Ce projet est sous licence MIT.
