# Variables pour le module VPC

# Région AWS pour le VPC
variable "aws_region" {
  description = "Région AWS où le VPC sera créé"
}

# CIDR du VPC
variable "vpc_cidr" {
  description = "Bloc CIDR pour le VPC"
}
