output "vpc_id" {
  value = aws_vpc.my_vpc.id
  description = "ID du VPC"
}

output "vpc_cidr_block" {
  value = aws_vpc.my_vpc.cidr_block
  description = "Bloc CIDR du VPC"
}
