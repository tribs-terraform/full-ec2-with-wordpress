# Variables globales pour le projet Terraform

# Région AWS
variable "aws_region" {
  description = "Région AWS pour le déploiement"
  default     = "us-east-1"
}

# CIDR du VPC
variable "vpc_cidr" {
  description = "Bloc CIDR pour le VPC"
  default     = "10.0.0.0/16"
}

# Type de l'instance EC2
variable "instance_type" {
  description = "Type de l'instance EC2"
  default     = "t2.micro"
}

# Nom de la clé pour l'instance EC2
variable "key_name" {
  description = "Nom de la clé pour l'instance EC2"
}

# Adresse e-mail pour les notifications SNS
variable "sns_email" {
  description = "Adresse e-mail pour les notifications SNS"
}

# Nom de la base de données RDS
variable "db_name" {
  description = "Nom de la base de données RDS"
}

# Nom d'utilisateur pour la base de données RDS
variable "db_username" {
  description = "Nom d'utilisateur pour la base de données RDS"
}

# Mot de passe pour la base de données RDS
variable "db_password" {
  description = "Mot de passe pour la base de données RDS"
}

# Nom de l'hôte de la base de données RDS
variable "db_host" {
  description = "Nom de l'hôte de la base de données RDS"
}

# Sous-domaine pour AWS Route 53
variable "route53_subdomain" {
  description = "Sous-domaine pour AWS Route 53"
  default     = "subdomain.domain.com"
}
