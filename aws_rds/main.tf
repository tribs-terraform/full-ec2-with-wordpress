# Utilisation d'AWS Secrets Manager pour stocker le mot de passe de la base de données
resource "aws_secretsmanager_secret" "db_password" {
  name = "db_password"  # Nom du secret dans Secrets Manager
}

# Version du secret pour le mot de passe de la base de données
resource "aws_secretsmanager_secret_version" "db_password" {
  secret_id     = aws_secretsmanager_secret.db_password.id  # ID du secret
  secret_string = var.db_password  # Valeur du mot de passe
}

# Configuration de l'instance RDS
resource "aws_db_instance" "default" {
  allocated_storage    = 20  # Espace de stockage alloué en GB
  storage_type         = "gp2"  # Type de stockage
  engine               = "mysql"  # Moteur de la base de données
  engine_version       = "5.7"  # Version du moteur
  instance_class       = "db.t2.micro"  # Classe d'instance
  name                 = var.db_name  # Nom de la base de données
  username             = var.db_username  # Nom d'utilisateur pour la base de données
  password             = aws_secretsmanager_secret_version.db_password.secret_string  # Mot de passe depuis Secrets Manager
  parameter_group_name = "default.mysql5.7"  # Groupe de paramètres
  skip_final_snapshot  = true  # Ignorer le snapshot final lors de la suppression de l'instance
}

# Output pour le point de terminaison RDS
output "db_host" {
  value = aws_db_instance.default.endpoint  # Point de terminaison de l'instance RDS
}
