output "rds_instance_id" {
  value = aws_db_instance.my_db.id
  description = "ID de l'instance RDS"
}

output "rds_endpoint" {
  value = aws_db_instance.my_db.endpoint
  description = "Point de terminaison de l'instance RDS"
}

output "rds_username" {
  value = aws_db_instance.my_db.username
  description = "Nom d'utilisateur de la base de données RDS"
}
