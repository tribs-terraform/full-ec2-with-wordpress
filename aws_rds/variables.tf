# Variables pour le module RDS

# Taille de stockage pour la base de données
variable "db_storage" {
  description = "Taille de stockage pour la base de données RDS"
}

# Type de stockage pour la base de données
variable "db_storage_type" {
  description = "Type de stockage pour la base de données RDS"
}

# Moteur de la base de données
variable "db_engine" {
  description = "Moteur de la base de données RDS"
}

# Version du moteur de la base de données
variable "db_engine_version" {
  description = "Version du moteur de la base de données RDS"
}

# Classe d'instance pour la base de données
variable "db_instance_class" {
  description = "Classe d'instance pour la base de données RDS"
}

# Nom de la base de données
variable "db_name" {
  description = "Nom de la base de données RDS"
}

# Nom d'utilisateur pour la base de données
variable "db_username" {
  description = "Nom d'utilisateur pour la base de données RDS"
}

# Mot de passe pour la base de données
variable "db_password" {
  description = "Mot de passe pour la base de données RDS"
}

# Groupe de paramètres pour la base de données
variable "db_parameter_group" {
  description = "Groupe de paramètres pour la base de données RDS"
}

# Ignorer le snapshot final lors de la suppression de la base de données
variable "db_skip_final_snapshot" {
  description = "Ignorer le snapshot final lors de la suppression de la base de
