# Configuration d'AWS CloudWatch
resource "aws_cloudwatch_log_group" "cloudwatch_log_group" {
  name = "cloudwatch_log_group"
}

# Alarme CloudWatch pour l'utilisation du CPU
resource "aws_cloudwatch_metric_alarm" "cpu_utilization_alarm" {
  alarm_name          = "cpu-utilization-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "70"
  alarm_description   = "Cette alarme déclenche lorsque l'utilisation du CPU dépasse 70% pendant une minute."
  alarm_actions       = [aws_sns_topic.cloudwatch_alarm.arn]
}

# Nouveau : Alarme CloudWatch pour l'utilisation de la mémoire
resource "aws_cloudwatch_metric_alarm" "memory_utilization_alarm" {
  alarm_name          = "memory-utilization-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryUtilization"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "Cette alarme déclenche lorsque l'utilisation de la mémoire dépasse 80% pendant une minute."
  alarm_actions       = [aws_sns_topic.cloudwatch_alarm.arn]
}

# Nouveau : Alarme CloudWatch pour le trafic réseau sortant
resource "aws_cloudwatch_metric_alarm" "network_out_alarm" {
  alarm_name          = "network-out-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkOut"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Sum"
  threshold           = "10000000"
  alarm_description   = "Cette alarme déclenche lorsque le trafic réseau sortant dépasse 10 MB pendant une minute."
  alarm_actions       = [aws_sns_topic.cloudwatch_alarm.arn]
}
