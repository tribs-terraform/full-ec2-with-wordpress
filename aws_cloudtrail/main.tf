# Configuration d'AWS CloudTrail
resource "aws_cloudtrail" "cloudtrail" {
  name                          = "cloudtrail"
  enable_logging                 = true
  include_global_service_events  = true
  is_multi_region_trail          = true
  enable_log_file_validation     = true
  s3_bucket_name                 = aws_s3_bucket.cloudtrail_bucket.bucket
}

# Bucket S3 pour CloudTrail
resource "aws_s3_bucket" "cloudtrail_bucket" {
  bucket = "cloudtrail-bucket"
  acl    = "private"
}
